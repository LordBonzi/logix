---
title: Companion App
---

<div class="p-notification--caution">
    <p class="p-notification__response">
        <span class="p-notification__status">Warning:</span>
        We are still in developing the Companion App for iOS and Android
    </p>
</div>

## We need really help, please follow us [here](developing)

# Android / F-Droid releases

You can find the latest release of the Companion App in the F-Droid repository:

[Companion App for the card10 Chaos Communication Camp badge](https://f-droid.org/app/de.ccc.events.badge.card10)

# iOS Testflight builds

**[https://testflight.apple.com/join/oqZhY70M](https://testflight.apple.com/join/oqZhY70M)**

 ⚠️ Please note ⚠️ We've got a lot of trouble pairing with the card10. If your want to connect to the card10, follow the instructions on the [development page](developing). 

[![https://testflight.apple.com/join/oqZhY70M](http://api.qrserver.com/v1/create-qr-code/?color=000000&bgcolor=FFFFFF&data=https%3A%2F%2Ftestflight.apple.com%2Fjoin%2FoqZhY70M&qzone=1&margin=0&size=400x400&ecc=L)](https://testflight.apple.com/join/oqZhY70M)
