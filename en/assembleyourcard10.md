---
title: Assemble your card10
---

Your card10 comes in a bag with all its components and a [flyer/booklet/manual](/media/assembly_flyer.pdf). You can also checkout our assembly [video-tutorial](/vid/).

The main components of your card10 are two electronic boards, the _fundamental_ board and the _harmonic_ board. 
The _fundamental_ board sits directly on the wrist band, the _harmonic_ board with the display sits on top of it.
If you are curious to learn more about the two boards, you can find a more detailed description on the [hardware overview](/en/hardware-overview/) page.
When unpacking and assembling, make sure to keep your protective foil on the display at first, until you had a look at the 
[polarizing display](/en/faq/#why-should-i-consider-keeping-the-protective-foil-on-the-display) faq section.

[![Harmonic board with glue dot](/media/assemble/IMG_20190819_143216.jpg)](/media/assemble/IMG_20190819_143216.jpg)

- use the glue dot to attach the battery to the harmonic board

[![Battery attached to harmonic board](/media/assemble/IMG_20190819_143128.jpg)](/media/assemble/IMG_20190819_143128.jpg)

- plug in the battery

[![Attaching the spacers](/media/assemble/IMG_20190819_142944.jpg)](/media/assemble/IMG_20190819_142944.jpg)

- attach the spacers with four of the screws
- put the nylon spacer next to LED3. The nylon spacer is not symmetrical, the flat side should face the fundamental board screw. Be careful not to fasten the screw too firmly to the nylon spacer, otherwise you risk damaging the thread in the spacer and the screw will become loose.

[![Both boards placed on top of each other](/media/assemble/IMG_20190819_143114.jpg)](/media/assemble/IMG_20190819_143114.jpg)

- carefully connect the two boards with the connector

[![Wristband with card10](/media/assemble/IMG_20190819_142436.jpg)](/media/assemble/IMG_20190819_142436.jpg)

- place the wristband with its fluffy side up, put the metal bars on top of the badge and add the remaining screws


[![Wristband with card10](/media/assemble/IMG_20190819_142653.jpg)](/media/assemble/IMG_20190819_142653.jpg)



## Have fun with your card10!
Now it's time to [switch on](/en/gettingstarted/#card10-navigation)
 your card10.

[![Happy Hacking!](/media/assemble/IMG_m6w9xw.jpg)](/media/assemble/IMG_m6w9xw.jpg)
