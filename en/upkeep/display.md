---
title: Display repair and replacement
---

All card10 displays were hand soldered by volunteers, many directly at cccamp2019 - impressive, right?!
On the other hand, if untrained volunteers were able to do it, you can probably learn how to solder a card10 display, too!
If you have little soldering experience, it might still be good to find someone with a bit of experience to guide you a bit.

Another consequence of the hand soldered displays: Particularly if your display never worked properly and is not visibly  damaged,
there is a good chance it
has a bad solder connection. So if your display appears broken, you might be able to fix it by just re-doing the solder connection.

This guidance shows you how to check your display, do this:

**Step 0:** Disassemble your card10 by removing the screws on the top PCB with a T6 screwdriver and gently pulling up the harmonic board.
Disconnect the battery with a pair of tweezers.

**Step 1:** The display sticks to the Harmonic board with two small double sided glue points (like the one you placed on
the backside to mount your battery). So careful lift the broken display with a pair of tweezers from the right side upwards.
Make sure that you under no circumstances lift the display to an extent so that the flex PCB of the display rips off
one or more solder pads from the harmonic PCB. Otherwise you would not be able to rework your Harmonic!

**Step 2:** Carefully remove the glue points from the Harmonic PCB.

If your display is not physically broken, now is a good time to check what the solder connection looks like.
If the solder connection looks bad, continue in section **Fix bad solder connection**, otherwise continue with **replacement**

### Fix bad solder connection
**Step 3:**
TODO

### Replacement

**Step 3:** Apply a larger amount of solder with your solder iron. For this step you want to use your largest
solder tip. Try to apply heat to all pins of the flex pcb at the same time. Slowly and carefully apply an
upward force to lift the flex pcb. If all of the pins are heated the flex pcb will come off the Harmonic board.

**Step 4:** Use a desolder wire to remove the rest of the solder from the Harmonic pcb. Be careful and make
sure not to damage the board by ripping off one of the pads.

**Step 5:** Place your new display in the right orientation onto your Harmonic board. Make sure that the small
holes of the display flex pcb and the Harmonic board pcb align. At this stage the metal can part of the display
(bottom side) will point upwards.

**Step 6:** Apply solder on the display flex pcb pins.

**Step 7:** Visually check the connections and make sure that there are no shorts between the pins.

**Step 8:** Apply two glue points to the Harmonic pcb (one directly over the blanc pins of the flex pcb to
isolate them to the metal can of the display and one on the right side of the display pcb marking). If you don't
have those glue points we used, you also can make use of standard double-sided tape.

**Step 9:** Flip the display over end carefully press the display to the Harmonic pcb. Make sure that the
force does not destroy the LCD. You can apply the force at the edges of the display. Also make sure that the LCD is
in its right position and aligned with the rectangle on the pcb. It's easy to produce shorts on the components on the
right edge of the LCD.

**Step 10:** Connect the battery and switch on the badge

The LCD can be bought at AliExpress: https://de.aliexpress.com/item/4000018872259.html


### tl;dr

**Parts needed**
Glue dots or double sided tape; replacement LCD, e.g. from AliExpress: https://de.aliexpress.com/item/4000018872259.html

**Step 0:** Disconnect the battery with a pair of tweezers.

**Step 1:** Carefully lift the broken display with a pair of tweezers from the right side upwards.
Make sure you don't rips off
one or more solder pads from the harmonic PCB. Otherwise you will not be able to rework your Harmonic!

**Step 2:** Remove the glue points from the Harmonic PCB.

**Step 3:** Apply a larger amount of solder with your solder iron with your largest
solder tip. Try to apply heat to all pins of the flex pcb at the same time. Slowly and carefully apply an
upward force to lift the flex pcb. If all of the pins are heated, the flex pcb will come off the Harmonic board.

**Step 4:** Use a desolder wire to remove the rest of the solder from the Harmonic pcb. Again, avoid ripping off one of the pads.

**Step 5:** Place your new display in the right orientation onto your Harmonic board. Make sure that the small
holes of the display flex pcb and the Harmonic board pcb align. At this stage the metal part of the display
(bottom side) will point upwards.

**Step 6:** Apply solder on the display flex pcb pins.

**Step 7:** Visually check the connections and make sure that there are no shorts between the pins.

**Step 8:** Use glue points or double sided tape to attach the display. Make sure to cover the blank pins of the flex pcb
with the points/tape to isolate them.

**Step 9:** Flip the display over, carefully press the display to the Harmonic pcb, ensure correct alignment to avoid
shorting other components on the PCB.

**Step 10:** Connect the battery and switch on the badge




