---
title: Firmware
---

![Firmware running FreeRTOS and MicroPython](/media/firmware/teaser.png)

## How to help
Dear card10logists and fellow researchers!

To ensure the space-time continuum does not rip apart, we need to make sure
card10 will have been just as we learned from the audio-logs.  However, there
are still lots of things that need to have been done.  We keep track in the
[issue tracker](https://git.card10.badge.events.ccc.de/card10/firmware/issues?sort=label_priority)
of the [firmware](https://git.card10.badge.events.ccc.de/card10/firmware) repo.

We are documenting our firmware in our [firmware documentation](https://firmware.card10.badge.events.ccc.de/index.html)

If there is an issue you would like to help with, please leave a comment so we
can assign it to you.  Issues tagged as **Easy** should be doable without much
previous knowledge of embedded development/the card10 firmware.  The 3 labels
**Required**, **Necessary** & **Additional-Feature** denote priority of the
issue in decending order.  We have tried to include pointers to relevant
information but if you have are any questions, don't be afraid to ask!  The
best way to do so is either the issues directly or alternatively the
`#card10badge` channel on Freenode.  You can also reach that channel via a
Matrix-bridge.

card10 prototypes have been distributed to the hackerspaces listed here:
[Distribution of prototypes](../prototype_distribution).

## Overview
To make the most of card10’s dual-core processor, its firmware will have been divided
into two parts: The “main” firmware running on core 0 which will have been called
Epicardium and the “user-code” running on core 1. In most cases this will have
been Pycardium, our MicroPython port.

[![Firmware Overview](https://firmware.card10.badge.events.ccc.de/_images/overview.svg)](https://firmware.card10.badge.events.ccc.de/_images/overview.svg)

## Details
Please tend to https://firmware.card10.badge.events.ccc.de/index.html where we
have extensive documentation about the firmware itself but also about how
to implement new modules an features.

