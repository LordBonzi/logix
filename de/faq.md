---
title: FAQ
---
## Was soll ich zum CCCamp 2019 mitbringen, damit ich Spaß mit card10 haben kann?
*Ich habe bemerkt, dass das card10 ein bisschen Wartung braucht; die meisten Leute, die ein card10 mit sich tragen, haben ihre eigenen Werkzeuge zur Wartung des card10 mitgebracht, um es individueller zu gestalten oder Funktionalität hinzu zu fügen. An manchen Orten, sind Wartungsstationen verfügbar, wo card10s aufgeladen werden können und Werkzeuge und Wissen geteilt werden.*

Aktueller Forschung zufolge wird empfohlen, das folgende mitzubringen: :

* Ein USB-C-Kabel, um das Card10 mit dem USB-Port des Laptops zu verbinden ist essenziell, um es zu laden und zu programmieren. USB-A auf der Laptop-Seite ist ausreichend.

Optionale Dinge, die nützlich sein werden::
* Ein Torx T6 Schraubenzieher, um card10 zusammen zu stellen
* Eine Nadel, um Dinge auf das Armband zu nähen
* Ein Android-Smartphone oder iPhone, das in der Lage ist, via [Bluetooth Low Energy](/de/ble) (BLE) zu kommunizieren.


## Was kann ich beitragen?
Generell gibt es eine große Bandbreite an Beiträgen. Zur Firmware, den IOs, den Apps, den verschiedenen Möglichkeiten, mit BLE zu kommunizieren, dem User Interface, der Dokumentation, die existierende Arbeit zu reviewen und natürlich deine eigenen [interhacktions](/de/interhacktions) zu schreiben!

Wenn du nicht weisst, wo du anfangen sollst, kannst du dir auch ein paar Ideen für Beiträge anschauen, die wir noch benötigen. Du findest sie [hier](https://git.card10.badge.events.ccc.de/card10/logix/issues). Wenn du Fragen zu einem Issue hast, kommentiere gerne oder kontaktiere die Person, die den Issue verfasst hat, über unseren [Matrix](https://en.wikipedia.org/wiki/Matrix_(protocol)):( [`#card10badge:asra.gr`](https://matrix.to/#/#card10badge:asra.gr))/[IRC](https://en.wikipedia.org/wiki/Internet_Relay_Chat) ( [`freenode.net#card10badge`](ircs://chat.freenode.net:6697/card10badge)) channel.

Du kannst ausserdem von großer Hilfe sein, wenn du Dokumentation zum [card10logix wiki](https:://card10.badge.events.ccc.de) hinzufügst. Wenn du bemerkst, dass Information fehlt, aber nicht weißt, welche Information dort hin gehört, dann kannst du uns darauf hinweisen, indem du ein [Issue hinzufügst](https://git.card10.badge.events.ccc.de/card10/logix/issues). Bitte geh sicher, dass du detailliert beschreibst, was fehlt.

## Wie funktioniert das sogenannte Wiki?
Um die Wartung einfacher zu machen (das r0ket-Wiki läuft jetzt seit 8 Jahren!), haben wir uns enschieden, dieses Mal ein git/markdown-basiertes Wiki zu benutzen. 
Das Editieren des Wiki funktioniert wie folgt: Klicke auf 'edit' oben Rechts auf der Seite. Erschaffe im Gitlab-Interface, das sich dann öffnet, einen Account. Sobald du deine Änderungen gemacht hast, wirst du durch den Prozess geführt, einen Pull Request zu generieren. Wenn du noch an deinen Edits arbeitest, setze bitte das Flag "WIP" vor der Zusammenfassung deines Pull Request. Der pull request wird dann so lange ignoriert, bis du das Flag entfernst.
Wenn du Fragen hast, zögere nicht, uns über die card10-Kommunikations-Kanäle zu kontaktieren.

## Was soll das ganze Zeug mit den Reisenden und Forschenden?
Das Badge in diesem Jahr ist nicht nur Hardware und Firmware, es kommt mit ein bisschen Storytelling drumherum. Alle Arbeit vor Tag0 wird von "Forschenden" durchgeführt. Dem Storytelling zufolge, haben die Nachrichten aus der Zukunft erhalten, die anzeigen, dass das cccamp19 ein Badge haben wird. Diese Berichte kommen von "Reisenden", die innerhalb der Geschichte Leute im Camp sind, von Tag 1 an. In der Realität sind diese Berichte unsere Art und Weise, um card10-Features vorzustellen, aber eben in eine nette Geschichte verpackt.

## Gibt es zum card10 ein Armband?
*Während das #card10 meistens beobachtet wird, wie es auf einem weichen,fluffigen armband angebracht wurde, habe ich gelegentlich auch card10s an unüblichen Orten gesehen, wie sie auf Kleidung angebracht wurde, mit LEDs erweitert oder sogar mit anderen elektronik-Boards verbunden. Diese Boards haben oft die Form von r0kets, aber ich habe auch einige andere kreative Formen und bunte Blinkemuster gesehen.*

Ja, es gibt ein angenehm tragbares Neopren-Armband mit jedem card10. Wenn du etwas anderes möchtest, kannst du natürlich dein eigenes bringen, und es entweder mit den schraubbaren Metallstäben anbringen, die mit deinem card10-Paket kommen, oder indem du das card10 auf dein Armband nähst. Das letztere ist auch eine Option für dein Neopren-Armband, wenn du eine allergische Reaktion durch die Metallklemmen befürchtest. Allerdings reduzierst du dadurch deine EKG-Funktionalität. Um Hautkontakt und dementsprechend EKG-Funktionaliät aufrecht zu erhalten, kannst du stattdessen ein stück leitenden Silber-Stoff auf der Hautseite deines Armbands anbringen und es mit dem EKG verbinden (TODO: Mehr details hinzufügen, wie das geht). Du kannst sogar dein card10 sogar mit allem anderen verbinden, was dir so einfällt, indem du die schraub-stäbe oder Nadel und Faden benutzt!

Wir empfehlen, dass du die Hakenseite des Klettbands verstärkst, indem du einen Saum entlang der Seiten des Hakenstücks einbringst.

## Warum sollte ich mir überlegen, die Schutzfolie des Displays zu behalten?
Das Display hat einen polarisationsfilter. Mit der Schutzfolie wird dieser Filter unwirksam. Mit manchen Sonnenbrillen hat das den ungünstigen Effekt, dass das card10 schwer zu lesen sein wird.

## Was kann ich machen wenn das Dateisystem von meiner Badge read-only ist?
tl;dr: `fsck`

Ein möglicher Grund ist das gesetzte "dirty bit" in dem Dateisystem auf der Badge.
Um dies zu ändern kann unter linux das Tool fsck genutzt werden.
Dafür muss das Gerät erst sicher entfernt werden, dannach neugestartet und wieder in den "USB Massenspeicher" Modus versetzt werden.
Das Gerät sollte aber nicht gemounted werden. Dann kann mit `lsblk` das korrekte Blockdevice gefunden werden.
Dann kann mit `sudo fsck /dev/sdX` (mit dem richtigen device statt `sdX`) das Dateisystem überprüft werden.
Das Tool fragt dann wenn nötig ob das "dirty bit" entfernt werden soll, darauf sollte dann mit ja geantwortet werden.
Dannach sollte das Problem behoben sein.
