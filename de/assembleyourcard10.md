---
title: Das card10 zusammenbauen
---

Dein card10 kommt mit all seinen Komponenten und einem [flyer/booklet/Handbuch](/media/assembly_flyer.pdf). Du kannst dir ausserdem unser Zusammenbau-[video-tutorial](/vid/) anschauen.

Die Hauptkomponenten deines Card10 sind zwei elektronische Boards, das _fundamental_-Board und das _harmonic_-Board.
Das _fundamental_-Board sitzt direkt auf dem Armband, das _harmonic_-Board sitzt darüber.
Wenn du mehr über die Boards lernen willst, kannst du eine detailliertere Beschreibung auf der [Hardware-Übersicht](/en/hardware-overview/)-Seite finden.
Wenn du die Tüte auspackst und das Board zusammensetzt, sorge dafür dass die Schutzfolie auf dem Display bleibt, bis du dir die Sektion zu 
[polarisierenden Displays](/de/faq/#why-should-i-consider-keeping-the-protective-foil-on-the-display) in der FAQ angeschaut hast.

[![Harmonic board with glue dot](/media/assemble/IMG_20190819_143216.jpg)](/media/assemble/IMG_20190819_143216.jpg)

- Benutze den Klebepunkt, um die Batterie auf das Harmonic-Board zu kleben

[![Battery attached to harmonic board](/media/assemble/IMG_20190819_143128.jpg)](/media/assemble/IMG_20190819_143128.jpg)

- Stecke die Batterie ein

[![Attaching the spacers](/media/assemble/IMG_20190819_142944.jpg)](/media/assemble/IMG_20190819_142944.jpg)

- Schraube die Abstandshalter mit den vier Schrauben an.
- Stecke den nylon-Abstandshalter neben LED3. Der Nylon-Abstandshalter ist nicht symmetrisch, die flache Seite sollte in Richtung der Fundamental-Board-Schraube zeigen. Sei vorsichtig und zieh die Schraube auf dem Nylon-Abstandshalter nicht zu fest an, sonst riskierst du, dass das Gewinde im Abstandshalter beschädigt wird und die Schraube sich lockert.

[![Both boards placed on top of each other](/media/assemble/IMG_20190819_143114.jpg)](/media/assemble/IMG_20190819_143114.jpg)

- Stecke die beiden Boards vorsichtig zusammen.

[![Wristband with card10](/media/assemble/IMG_20190819_142436.jpg)](/media/assemble/IMG_20190819_142436.jpg)

- lege das Armband hin, mit der flauschigen Seite nach oben, und lege die Metall-stäbe auf das Badge und schraube die übrigen Schrauben an.


[![Wristband with card10](/media/assemble/IMG_20190819_142653.jpg)](/media/assemble/IMG_20190819_142653.jpg)



## Viel Spass mit deinem card10!
Jetzt ist es Zeit, dein card10 [anzuschalten](/de/gettingstarted/#card10-navigation).

[![Happy Hacking!](/media/assemble/IMG_m6w9xw.jpg)](/media/assemble/IMG_m6w9xw.jpg)
