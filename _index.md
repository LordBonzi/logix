---
title: Home
---
# News

## Firmware Update: Okra (as of 2020-02-02)
It is very easy to [update your card10 firmware](/en/firmwareupdate) with just a USB-C cable. The update instructions
also contain a list of the existing firmware releases.

## Hello 36C3

Our assembly is here: [c3nav](https://36c3.c3nav.de/l/card10/@0,491.08,374.33,5)

Please stop by at any time :)


We are working on a workshop schedule and post it on the [workshop page](/en/workshops/)

## Camp was wonderful, see you at 36C3
if you still have a voucher (white or blue), bring it to congress and you might be able to redeem it against one of the card10s we are reparing in the meantime.

No sale of card10s!

## Got a card10 you don't use or need one for your next project?
The card10 exchange point is [here](/en/card10exchange)

# card10

Welcome to card10 Wiki. You can [browse](https://git.card10.badge.events.ccc.de/card10/logix) this wiki source and [read more](/en/gitlab) about our GitLab instance.

**card10** is the name of the badge for the 2019 [Chaos Communication Camp](https://events.ccc.de/camp/2019/wiki/Main_Page). 

### [Getting started](/en/gettingstarted) 
You just received your card10, what now? The [Getting started](/en/gettingstarted) page has some advice how to assemble and use the card10.

### [First interhacktions](/en/firstinterhacktions)
After playing around with card10 a bit, you want to learn how to write your own software. Even if you have never programmed before, it is super easy to make some LEDs blink on card10 :)

### [tl;dr](/en/tldr)
Spare me the details just give me the links to the docs!

### Overview

[![Photo of the card10 badge](media/ECKyISOXsAYDJZl.jpg)](media/ECKyISOXsAYDJZl.jpg)
>
> Hello, my name is card10

### Community
  - [IRC](https://en.wikipedia.org/wiki/Internet_Relay_Chat): [`freenode.net#card10badge`](ircs://chat.freenode.net:6697/card10badge)
  - or [Matrix](https://en.wikipedia.org/wiki/Matrix_(protocol)) (mirror): [`#card10badge:asra.gr`](https://matrix.to/#/#card10badge:asra.gr)
  - [GitLab: `https://git.card10.badge.events.ccc.de`](https://git.card10.badge.events.ccc.de/explore/groups/)

### Social Media
  - [`@card10badge@chaos.social`](https://chaos.social/@card10badge)
  - [`twitter.com/card10badge`](https://twitter.com/card10badge)

### [FAQ](/en/faq)

### Events
  - Upcoming:
    - 36C3, meet other card10 hackers at the card10 assembly!
  - Events [archive](en/events).
